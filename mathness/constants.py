# -*- coding: utf-8 -*-
import logging

import mpmath as mp

log = logging.getLogger(name=__name__)


def pi(dps=15):
    """
    Give a precise value for pi

    Parameters
    ----------
    dps : int, optional
        How many decimal points of precision
        Default: 15

    Returns
    -------
    mpmath object
        pi
    """
    with mp.workdps(dps):
        return (
            mp.exp(-mp.dirichlet(0, [0, 1, 0, -1], 1)) *
            mp.gamma(0.25) ** 2 / (2 * mp.sqrt(2))
        )
