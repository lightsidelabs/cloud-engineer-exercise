# -*- coding: utf-8 -*-
import logging

import sympy

log = logging.getLogger(name=__name__)


def sin_times_e_to_the(val):
    """
    Returns the derivative of sin(x) * e^x

    Parameters
    ----------
    val : float
        The value of x

    Returns
    -------
    float
    """
    x = sympy.symbols('x')
    d = sympy.diff(sympy.sin(x) * sympy.exp(x), x)

    return d.evalf({x: val})
