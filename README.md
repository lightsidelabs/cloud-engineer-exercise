# README: Turnitin Pittsburgh Cloud Engineer Coding Exercise

Welcome! We're excited you've decided to talk with us about a position on our
research team.

The purpose of this exercise is so we can have a conversation about your
technical background and abilities. We think that asking you to code on a
whiteboard during an interview isn't a great way to have a conversation. And
even if we sit down and pair during an interview it's a higher pressure
situation than it could be.

Instead we ask that you read these instructions and do a few hours of work,
on your time, to complete the exercise. During the interview we'll talk about
decisions you've made, the resulting application, and how you might change it
given different circumstances.

All of these tasks should be accomplishable within the Free Tier on Amazon
Web Services. If that is not true for you (i.e. you've used up all of your Free
Tier already), please let us know and we'll work something out.

Below are two sections:

* *Instructions*: the problem we'd like you to solve along with expectations we
  have about your solution.
* *Logistics*: constraints around the problem, and how we'd like you to
  communicate your solution to us

# Instructions

## Problem

We have a set of python modules that we would like to wrap up into a single,
installable package. Those modules and submodules can be found in the `mathness`
folder of this repository.

To make this package usable, we would like you to write two things:

__1.__ A `setup.py` file. The package name should be `mathness`. Other details
are left to your discretion.

__2.__ A `Dockerfile` that makes this package available in a container. By that,
we mean that if we were to run a python interpreter in the container created by
this Dockerfile, we would be able to successfully `import mathness`.

Easy-peasy, right? Unfortunately, we have a (intentionally) complicated method
by which we would like you to send us these files. We have built a state machine,
using Amazon Step Functions, that queries for the location of some s3 asset and
then copies it to an s3 bucket that we own. We'd really like to reuse this
code as our submission mechanism. In order to do so, we need you to do the
following things:

__3.__ First, we would like you to upload a zip of the two files to an s3
location of your choosing.

__4.__ Then, we need you to write an Amazon Lambda function that will tell us where to
look for the zip file. This function simply returns a string of the form
`bucket-name/key-name`

Our Step Functions state machine will call your Lambda function for this
information, and then copy that zip file into our s3 bucket.


# Logistics

__1.__ Timeframe

You should take a max of six hours to complete this exercise. We want to be
both respectful of your time and fair to other candidates who might not have
a whole week to work on it. Realistically, it should take you less than six
hours to complete all of the tasks.

__2.__ Language

Please write your Lambda function in python.

__3.__ Access

Our Lambdas that perform the work of our Step Functions state machine will need
permission to:

* Call _your_ Lambda function
* Copy the file you have uploaded to s3

Additionally, we would like to be able to see your Lambda function, and will
need permission for the `lambda:GetFunction` action.

Please grant account `733718951373` the appropriate permissons to do these things.
Please _do not_ provide public access to them.

_NOTE: This is probably not best practice for granting machine-to-machine access
across accounts. If you have thoughts about how we should have set this up, we'd
love to talk about that in your interview, but don't get too hung up on it now._

__4.__ Submission

In order to submit your zip file, we want you to run our Step Functions state machine.
The information you need two pieces of information:

* ARN of the state machine: `arn:aws:states:us-east-1:733718951373:stateMachine:CloudEngineerExercise`
* A role that can access the state machine: (see below)

The required input to this state machine is a dictionary with the following info:
```
{
  "email": your email address,
  "lambda_arn": the ARN of your Lambda function
}
```

In order for us to create a role that will let you access our Step Functions state
machine, we need to know your AWS account ID. If you would please email your AWS
account ID to `smiel@turnitin.com`, we will create a role for you and send you
the ARN that you can use to assume the role.

_NOTE: If you were already working for us, perhaps you could have helped us set up some
sort of federated web identity role for this instead of having to hand-craft user
roles! ;)_

You can submit as many times as you would like, but please send an email to
`smiel@turnitin.com` to let us know once you have run your final submission.

**Warning: AWS Step Functions are a fairly new offering. Please be sure you are
using the latest version of the AWS command line tools or SDK.**

__5.__ Debugging

Debugging cloud-based applications is tricky. Debugging them when your only access
is a vague description and a few endpoints is darn near impossible. We apologize
for that.

In effort to make things as clear as we can, we've provided the JSON of the state
machine in this repo in the file `state-machine.json`. You are not expected to
do anything with this, but it is there for you to inspect if you'd like.

As mentioned above, you are free to run the state machine at
`arn:aws:states:us-east-1:733718951373:stateMachine:CloudEngineerExercise`
as many times as you would like. The role provided allows you
to start the execution (`sfn:StartExecution`), describe the execution
(`sfn:DescribeExecution`), and view the execution history (`sfn:GetExecutionHistory`).
The value in the `output` field will contain a dictionary
with the inputs to the execution and the data passed back from your Lambda function.
It will also contain a field `success` which will either be True or False. If
it is True, then your files were properly submitted. If it is False, there was
an error somewhere in the process that was handled.

In the event of a unhandled failure, there will be error messages in the
`errors` field, and the `success` field may or may not be present.

__6.__ Non-required Code

You are not required to, but if you wind up writing any tests or scripts to assist
you in any of this work, please either:

* upload them to a private repository and grant `smiel@turnitin.com` read access
* zip them up and email them along with your final submission notification to
`smiel@turnitin.com`

__7.__ Questions

Feel free to contact `smiel@turnitin.com` with any questions you have. We can't
provide hints, but will work to clear up anything we have not sufficiently
described here.
